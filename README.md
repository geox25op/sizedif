# sizedif
is a Unix command-line utility program written in C that accepts 2 arguments
as file names, and calculates the size difference in bytes between them.
Sizedif also accepts "--help" as the first and only argument, and will
display a bit of information about the program.
## Compatibility
By default, sizedif should work without installing any new programs because
it only uses the commands [cat](https://en.wikipedia.org/wiki/Cat_(Unix)) and
[wc](https://en.wikipedia.org/wiki/Wc_(Unix)).
